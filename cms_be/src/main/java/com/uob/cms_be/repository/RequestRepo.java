package com.uob.cms_be.repository;

import com.uob.cms_be.domain.Request;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepo extends CrudRepository<Request, Long> {
}
