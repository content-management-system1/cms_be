package com.uob.cms_be.repository;

import com.uob.cms_be.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo extends CrudRepository<User, Long> {
    boolean existsByUsername(String username);

    @Query("select u from User u where username like %?1%")
    public List<User> searchByUsername (String username);

    User findByUsernameAndPassword(String username, String password);
}
