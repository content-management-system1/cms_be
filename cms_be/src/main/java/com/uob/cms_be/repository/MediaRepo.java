package com.uob.cms_be.repository;

import com.uob.cms_be.domain.Media;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MediaRepo extends CrudRepository<Media, Long> {
    Optional<Media> findByName(String name);

    @Query("select m from Media m where title like %?1%")
    public List<Media> searchByTitle (String title);
}
