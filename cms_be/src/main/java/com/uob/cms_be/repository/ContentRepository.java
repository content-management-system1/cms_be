package com.uob.cms_be.repository;

import com.uob.cms_be.domain.Category;
import com.uob.cms_be.domain.Content;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ContentRepository extends CrudRepository<Content, Long> {

}
