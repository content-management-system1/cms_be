package com.uob.cms_be.controller;

import com.uob.cms_be.domain.ImageUploadResponse;
import com.uob.cms_be.domain.Media;
import com.uob.cms_be.service.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping("/media")
public class MediaController {
    @Autowired
    MediaService mediaService;

    //get all media -ok
    @GetMapping("/get-all")
    public ResponseEntity<?> getAllMedia() {
        List<Media> mediaList = mediaService.getAllMedia();
        return ResponseEntity.status(HttpStatus.OK)
                .body(mediaList);
    }

    //search image by name
    @GetMapping("/search")
    public ResponseEntity<?> getAllByTitle(@RequestParam("title") String title){
        List<Media> mediaList = mediaService.searchByTitle(title);
        return ResponseEntity.status(HttpStatus.OK)
                .body(mediaList);
    }

    //upload media -ok
    @PostMapping("/upload")
    public ResponseEntity<?> uploadImage(@RequestParam("image") MultipartFile file, @RequestParam("title") String title) throws IOException {
        ImageUploadResponse response = mediaService.uploadImage(file, title);
        return ResponseEntity.status(HttpStatus.OK)
                .body(response);
    }

    //get info by name - ok
    @GetMapping("/get-info/name/{name}")
    public ResponseEntity<?>  getImageInfoByName(@PathVariable("name") String name){
        Media imageList = mediaService.getInfoByImageByName(name);

        return ResponseEntity.status(HttpStatus.OK)
                .body(imageList);
    }

    //get info by id - ok
    @GetMapping("/get-info/id/{id}")
    public ResponseEntity<?>  getImageInfoById(@PathVariable("id") Long id){
        Media image = mediaService.getInfoByImageById(id);

        return ResponseEntity.status(HttpStatus.OK)
                .body(image);
    }


    //get image by name
    @GetMapping("/get-image/name/{name}")
    public ResponseEntity<?>  getImageByName(@PathVariable("name") String name){
        byte[] image = mediaService.getImageByName(name);

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(image);
    }

    //get image by id
    @GetMapping("/get-image/id/{id}")
    public ResponseEntity<?>  getImageById(@PathVariable("id") Long id){
        byte[] image = mediaService.getImageById(id);

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(image);
    }




    //delete image by id
    @DeleteMapping("delete/{id}")
    public String deleteMedia(@PathVariable Long id){
        return mediaService.deleteMedia(id);
    }

}
