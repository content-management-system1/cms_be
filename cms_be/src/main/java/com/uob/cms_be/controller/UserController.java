package com.uob.cms_be.controller;

import com.uob.cms_be.domain.User;
import com.uob.cms_be.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserRepo userRepo;

    //create user
    @PostMapping("/create")
    public User createUser(@RequestBody User user){
        if(userRepo.existsByUsername(user.getUsername())) {
            throw new IllegalArgumentException("Username already exists.");
        }
        userRepo.save(user);
        return user;
    }

    //edit user details by id
    @PostMapping("/edit")
    public User editUser(@RequestBody User userNew){
        Optional<User> opt = userRepo.findById(userNew.getUserId());
        if (opt.isPresent()){
            User userOri = opt.get();
            userOri.setUsername(userNew.getUsername());
            userOri.setPassword(userNew.getPassword());
            userOri.setEmail(userNew.getEmail());
            userOri.setRole(userNew.getRole());
            userRepo.save(userOri);
            return userOri;
        } else {
            return new User();
        }
    }

    //get all user
    @GetMapping("/get-all")
    public Iterable<User> getAllUser(){
        return userRepo.findAll();
    }

    //get user by id
    @GetMapping("/id/{id}")
    public User findUserById (@PathVariable("id") Long id){
        Optional<User> opt = userRepo.findById(id);
        return opt.orElseGet(User::new);
    }

    //search by username keyword
    @GetMapping("/search")
    public List<User> searchByUsername (@RequestParam String username){
        return userRepo.searchByUsername(username);
    }

    //delete user by id
    @DeleteMapping("/delete/{id}")
    public String deleteUser(@PathVariable Long id){
        Optional<User> user = userRepo.findById(id);
        if(user.isPresent()){
            User temp = user.get();
            userRepo.delete(temp);
            return "deleted";
        } else {
            return "no user found";
        }
    }

    //login
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody User user) {
        String username = user.getUsername();
        String password = user.getPassword();

        // Search for the user in the repository
        User foundUser = userRepo.findByUsernameAndPassword(username, password);

        if (foundUser != null) {
            // User found, return success response
            return ResponseEntity.ok(foundUser);
        } else {
            // User not found, return error response
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid username or password");
        }
    }
}
