package com.uob.cms_be.controller;

import com.uob.cms_be.domain.Request;
import com.uob.cms_be.repository.RequestRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class RequestController {

    @Autowired
    private RequestRepo requestRepo;

    @PostMapping("/add-request")
    public Request addRequest(@RequestBody Request request) {
        return requestRepo.save(request);
    }

    @GetMapping("/all-request")
    public Iterable<Request> getAllRequest() {
        return requestRepo.findAll();
    }

    @DeleteMapping("/ ")
    public String delete(@PathVariable Long id) {
        Optional<Request> rqt = requestRepo.findById(id);
        if(rqt.isPresent()){
            Request temp = rqt.get();
            requestRepo.delete(temp);
            return "ok";
        } else {
            return "no";
        }

    }



}
