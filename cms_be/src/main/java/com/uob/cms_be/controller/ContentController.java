package com.uob.cms_be.controller;

import com.uob.cms_be.domain.Category;
import com.uob.cms_be.domain.Content;
import com.uob.cms_be.domain.Media;
import com.uob.cms_be.repository.ContentRepository;
import com.uob.cms_be.service.ContentService;
import com.uob.cms_be.service.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/contents")
public class ContentController {

    @Autowired
    private ContentService contentService;
    @Autowired
    private MediaService mediaService;


    @GetMapping("/all")
    public Iterable<Content> getAllContents() {
        return contentService.getAllContents();
    }

    @GetMapping("/{contentId}")
    public Content getContentById(@PathVariable Long contentId) {
        return contentService.getContentById(contentId);
    }

    @PostMapping("/create/{id}")
    public Content createContent(@RequestBody Content content, @PathVariable Long id) {
        return contentService.createContent(content,id);
    }

    @PutMapping("/{contentId}")
    public Content updateContent(@PathVariable Long contentId, @RequestBody Content contentDetails) {
        return contentService.updateContent(contentId, contentDetails);
    }

    @DeleteMapping("/{contentId}")
    public void deleteContent(@PathVariable Long contentId) {
        contentService.deleteContent(contentId);
    }


}
