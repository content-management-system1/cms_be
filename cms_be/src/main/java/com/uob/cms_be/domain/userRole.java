package com.uob.cms_be.domain;

public enum userRole {
    PUBLISHER,
    ADMIN
}
