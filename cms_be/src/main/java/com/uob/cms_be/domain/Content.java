package com.uob.cms_be.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table
@NoArgsConstructor
        public class Content {

            @Id
            @GeneratedValue  (strategy = GenerationType.IDENTITY)
            private Long contentId;
            private String title;
            private String description;

            @JoinColumn (name = "user_id")
            @ManyToOne
            private User userId; //Referenced from User
            private Date updatedAt;

            @Enumerated(EnumType.STRING)
            private Category category;

            private Boolean status;

        @JsonIgnore
            @Lob
            @Column(name = "image", length = 100000)
            private byte[] image;

        }
