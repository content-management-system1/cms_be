package com.uob.cms_be.domain;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Media {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long mediaId;

    private String title;
    private String name;
    private LocalDateTime addedAt;
    private String type;

    @Lob
    @Column(name = "image", length = 100000)
    private byte[] image;

    //Created to reference class to User
//    @OneToMany(mappedBy = "media")
//    private Set<UserMedia> postedBy;
}
