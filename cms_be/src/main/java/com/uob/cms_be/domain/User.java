package com.uob.cms_be.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    private String username, password, email;

    @Enumerated(EnumType.STRING)
    private userRole role;

//    @OneToMany(mappedBy = "user")
//    private Set<UserMedia> postedMedia;

}
