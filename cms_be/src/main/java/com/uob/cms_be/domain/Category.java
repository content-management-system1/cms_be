package com.uob.cms_be.domain;

public enum Category{
    HOME,
    ABOUT_US,
    CONTACT
}
