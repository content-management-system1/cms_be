package com.uob.cms_be.service;

import com.uob.cms_be.domain.ImageUploadResponse;
import com.uob.cms_be.domain.Media;
import com.uob.cms_be.repository.MediaRepo;
import com.uob.cms_be.util.ImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class MediaService {
    @Autowired
    private MediaRepo mediaRepo;

    public ImageUploadResponse uploadImage(MultipartFile file, String title) throws IOException {

        mediaRepo.save(Media.builder()
                .title(title)
                .name(file.getOriginalFilename())
                .type(file.getContentType())
                .addedAt(LocalDateTime.now())
                .image(ImageUtil.compressImage(file.getBytes())).build());

        return new ImageUploadResponse("Image uploaded successfully: " +
                file.getOriginalFilename());

    }

    @Transactional
    public Media getInfoByImageById(Long id) {
        Optional<Media> dbImage = mediaRepo.findById(id);
        if (dbImage.isPresent()){
            return Media.builder()
                    .mediaId(dbImage.get().getMediaId())
                    .name(dbImage.get().getName())
                    .type(dbImage.get().getType())
                    .title(dbImage.get().getTitle())
                    .addedAt(dbImage.get().getAddedAt())
                    .image(ImageUtil.decompressImage(dbImage.get().getImage())).build();

        } else {
            return new Media();
        }
    }


    @Transactional
    public Media getInfoByImageByName(String name) {
        Optional<Media> dbImage = mediaRepo.findByName(name);
        if (dbImage.isPresent()){
            return Media.builder()
                    .name(dbImage.get().getName())
                    .type(dbImage.get().getType())
                    .image(ImageUtil.decompressImage(dbImage.get().getImage())).build();

        } else {
            return new Media();
        }
    }

    @Transactional
    public byte[] getImageByName(String name) {
        Optional<Media> dbImage = mediaRepo.findByName(name);
        if (dbImage.isPresent()){
            byte[] image = ImageUtil.decompressImage(dbImage.get().getImage());
            return image;
        } else {
            return null;
        }

    }

    @Transactional
    public byte[] getImageById(Long id) {
        Optional<Media> dbImage = mediaRepo.findById(id);
        if (dbImage.isPresent()){
            byte[] image = ImageUtil.decompressImage(dbImage.get().getImage());
            return image;
        } else {
            return null;
        }

    }



    //get all
    @Transactional
    public List<Media> getAllMedia() {
        return (List<Media>) mediaRepo.findAll();
    }

    @Transactional
    public List<Media> searchByTitle(String title) {
        return (List<Media>) mediaRepo.searchByTitle(title);

    }

    @Transactional
    public String deleteMedia(Long id) {
        mediaRepo.deleteById(id);
        return "deleted";
    }
}
