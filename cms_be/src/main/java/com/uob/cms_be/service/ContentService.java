package com.uob.cms_be.service;

import com.uob.cms_be.domain.Category;
import com.uob.cms_be.domain.Content;
import com.uob.cms_be.domain.Media;
import com.uob.cms_be.domain.User;
import com.uob.cms_be.repository.ContentRepository;
import com.uob.cms_be.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Date;
import java.util.Optional;

@Service
public class ContentService {

    @Autowired
    private ContentRepository contentRepository;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private MediaService mediaService;

    public Iterable<Content> getAllContents() {
        return contentRepository.findAll();
    }

    public Content getContentById(Long contentId) {
        Optional<Content> content = contentRepository.findById(contentId);
        if(content.isPresent()) {
            return content.get();
        } else {
            throw new RuntimeException("Category not found with id " + contentId);
        }
    }

    public Content createContent(Content content, Long id) {
        Long userId = content.getUserId().getUserId();
        User user = userRepo.findById(userId).orElseThrow(() -> new IllegalArgumentException("Invalid user id: " + userId));
        content.setUserId(user);
        content.setImage(mediaService.getImageById(id));
        return contentRepository.save(content);
    }

    public Content updateContent(Long contentId, Content contentDetails) {
        Content content = getContentById(contentId);
        content.setTitle(contentDetails.getTitle());
        content.setDescription(contentDetails.getDescription());
        content.setCategory(contentDetails.getCategory());
        content.setStatus(contentDetails.getStatus());
        content.setUpdatedAt(new Date());
        return contentRepository.save(content);
    }

    public void deleteContent(Long contentId) {
        Content content = getContentById(contentId);
        contentRepository.delete(content);
    }

}
